/*
 * mini-cp is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License  v3
 * as published by the Free Software Foundation.
 *
 * mini-cp is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY.
 * See the GNU Lesser General Public License  for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with mini-cp. If not, see http://www.gnu.org/licenses/lgpl-3.0.en.html
 *
 * Copyright (c)  2018. by Laurent Michel, Pierre Schaus, Pascal Van Hentenryck
 */


package minicp.engine.constraints;

import minicp.cp.Factory;
import minicp.engine.core.AbstractConstraint;
import minicp.engine.core.BoolVar;
import minicp.engine.core.Constraint;
import minicp.engine.core.IntVar;
import minicp.util.exception.NotImplementedException;

import java.util.ArrayList;
import java.util.Arrays;

import static minicp.cp.Factory.*;

/**
 * Disjunctive constraint with reified constraints decomposition.
 */
public class DisjunctiveDecomposition extends AbstractConstraint {

    private final BoolVar[][][] b;
    private final ArrayList<IntVar>[] startOnMachine;
    ArrayList<Integer>[] durationsOnMachine;
    private final int nMachines;
    private final int nJobs;

    /**
     * Creates a disjunctive constraint with reified constraints decomposition.
     *
     * @param b the boolean variable of each pair of activities by machine
     * @param startOnMachine the start time of each activities by machine
     * @param durationsOnMachine the duration of each activities by machine
     */
    public DisjunctiveDecomposition(BoolVar[][][] b, ArrayList<IntVar>[] startOnMachine, ArrayList<Integer>[] durationsOnMachine) {
        super(b[0][1][0].getSolver());
        this.b = b;
        this.startOnMachine = startOnMachine;
        this.durationsOnMachine = durationsOnMachine;
        this.nMachines = b.length;
        this.nJobs = b[0].length;
    }

    @Override
    public void post() {

        for (int m = 0; m < nMachines; m++) {
            for (int i = 0; i < nJobs; i++) {
                for (int j = i+1; j < nJobs; j++) {
                    getSolver().post(new IsLessOrEqualVar(b[m][i][j], plus(startOnMachine[m].get(i), durationsOnMachine[m].get(i)), startOnMachine[m].get(j)));
                    getSolver().post(new IsLessOrEqualVar(b[m][j][i], plus(startOnMachine[m].get(j), durationsOnMachine[m].get(j)), startOnMachine[m].get(i)));
                    getSolver().post(notEqual(b[m][i][j], b[m][j][i]));
                }
            }
        }
    }

    /**
     * ====================== To eventually add to Factory =====================
     * Returns a constraint imposing that a boolean variable is
     * equal to a given value true or false.
     *
     * @param x the boolean variable to be assigned to v
     * @param v the value that must be assigned to x
     * @return a constraint so that {@code x = v}
     */
    public static Constraint equalBoolVar(BoolVar x, boolean v) {
        return new AbstractConstraint(x.getSolver()) {
            @Override
            public void post() {
                x.fix(v);
            }
        };
    }



    /**
     * @return true if each of the b vars is fixed
     */
    public boolean isAllReifiedFixed(){
        boolean allAreFixed = true;

        for (int m = 0; m < nMachines; m++) {
                for (int i = 0; i < nJobs; i++) {
                        for (int j = i+1; j < nJobs; j++) {
                            if(!b[m][i][j].isFixed()){
                                allAreFixed = false;
                                break;
                            }
                        }
                    if(!allAreFixed) break;
                }
            if(!allAreFixed) break;
        }

        return allAreFixed;
    }


    public void fixStartVars(){
        //Fix each of the startOnMachine vars to its minimum value.
        //To use, for example, when all b vars are already fixed.
        for(int m = 0; m < nMachines; m++){
            for(int i = 0; i < nJobs; i++){
                IntVar s = startOnMachine[m].get(i);
                if(s.size() > 1)
                    getSolver().post(equal(s, s.min()));
            }
        }
    }

    /**
     * @return a non fixed bij such as the startOnMachine_i and startOnMachine_j has the smallest domain
     */
    public BoolVar bijToFix_v0(){

        int[][] sortIdx = new int[nMachines][nJobs];
        //will keep the sorted indexes ordered by size of the domains of startOnMachine variable
        for (int m = 0; m < nMachines; m++)
            for (int i = 0; i < nJobs; i++)
                sortIdx[m][i] = i;


        int exch = 0;
        int tmp = 0;
        for (int m = 0; m < nMachines; m++){
            for (int i = 1; i < nJobs; i++) {
                tmp = i;
                while (tmp > 0 &&
                        startOnMachine[m].get(sortIdx[m][tmp - 1]).size() >
                                startOnMachine[m].get(sortIdx[m][tmp]).size()) {
                    exch = sortIdx[m][tmp - 1];
                    sortIdx[m][tmp - 1] = sortIdx[m][tmp];
                    sortIdx[m][tmp] = exch;
                    tmp--;
                }
            }
            //sortIdx[km][i] is such that
            // startOnMachine[km].get(sortIdx[km][i]).size()) < startOnMachine[km].get(sortIdx[km][i+1]).size()
        }

        int midx = -1;
        int iidx = -1;
        int jidx = -1;

        for (int m = 0; m < nMachines; m++){
            //Identifying the non-fixed b_ij variable
            // with the startOnMachine_i and startOnMachine_j that
            // have the minimum startOnMachine domain size.
            //The machines are considered on a static way.
            for (int i = 0; i < nJobs; i++) {
                for (int j = i+1; j < nJobs; j++) {
                    if (!b[m][sortIdx[m][i]][sortIdx[m][j]].isFixed()){
                        midx = m;
                        iidx = sortIdx[m][i];
                        jidx = sortIdx[m][j];
                        break;
                    }
                }
                if(midx != -1) break;
            }
            if(midx != -1) break;
        }

        if(startOnMachine[midx].get(iidx).size() > startOnMachine[midx].get(jidx).size())
            return b[midx][iidx][jidx];
        else
            return b[midx][jidx][iidx];

    }



}
